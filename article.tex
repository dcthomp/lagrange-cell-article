\section{Introduction}

For many years, VTK's existing cells -- which interpolate linearly along an
axis~\footnote{Note that 2-D bilinear and 3-D trilinear cells are not truly linear;
they are quadratic or cubic but only vary linearly when constrained to motion parallel to an axis.}
-- have served the vast majority of use cases.
These linear cells are said to have \emph{order} or \emph{degree} 1.
They generally have $2$ points ($= \mbox{\emph{order}} + 1$) along each coordinate axis.
VTK has other cells that are nonlinear but have a fixed degree (e.g., \vtk{QuadraticQuad}).
However, spectral finite-element simulations are becoming prevalent;
these simulations can increase the degree of the interpolation arbitrarily
within a cell to increase accuracy.
This is different from more traditional methods where cells with low accuracy
are split geometrically so that they can
resolve finer spatial features to better approximate the solution.
VTK's existing quadratic and cubic cell can deal with some solvers that produce
higher-order cells, but are often not enough to deal with spectral solvers which
can increase the polynomial degree of a cell any number of times.

\begin{figure}[tbh]
  \centering
  \includegraphics[width=3.2in]{curvy-wedges}
  \caption{A regular grid of wedges that has been warped and then isocontoured and clipped.}
\end{figure}

We have just completed changes to VTK and ParaView that allow us to render approximations
to curves, triangles, quadrilaterals, tetrahedra, hexahedra, and wedges of any order up
to 10 (and to any order beyond that with simple, compile-time changes).
We use linear approximations for many tasks such as contouring and clipping,
but provide methods for exact interpolation and for adaptive tessellation
so that users can take full advantage of the smooth nature of these elements
when applications require it:

\begin{itemize}
\item If you use the \vtk{UnstructuredGridGeometryFilter} to extract the surface of
a mesh containing volumetric Lagrange cells, you will obtain lower-dimensional
Lagrange cells of the same order (e.g., if you have a tri-quartic hexahedron,
asking for one of its faces returns a bi-quartic quadrilateral).

\item Other geometry filters (e.g., \vtk{DataSetSurfaceFilter}) will either call each
cell's \meth{Triangulate} method, which returns a set of regular triangles that
connect the cell's points, or methods that approximate the cell with a set of
linear cells (which may be triangles, quadrilaterals, or tetrahedra).

\item Since graphical representations of datasets from the filters above can look
blocky and not reveal the true shape of the polynomial, you may want to use
the \vtk{TessellatorFilter} to generate smoother approximations to the polynomial.
This class adaptively subdivides edges according to tolerances you specify on
the chord error and field value differences.
\end{itemize}

Currently,

\begin{itemize}
\item the order of interpolation along each axis must be the same for all axes of the cell;
it is not possible to construct a quadrilateral that is cubic in one parameter and quartic in the other.

\item the connectivity for each Lagrange cell is a list of all the points where a value is
specified, whether they are corner or intermediate points.
Thus, in addition to storing x-y-z coordinate-data and point-field values for each point,
unstructured grids must store the offset into data arrays for each point.
The overhead for this increases quickly with order, e.g., $(N+1)^3$ for hexahedra of order $N$.
\end{itemize}

In the future, it is possible
for both the order to be specified per axis and
for the connectivity storage to be condensed to a fixed size per cell shape.
If the order is explicitly specified instead of inferred from the number of points defining a cell,
then we can store all points along a given edge or face together and specify
(1) the offset to the first point and (2) the organization of the points in memory
(i.e., which order are the points iterated over relative to the cell's parametric coordinate system).
We have taken the first step in this direction by keeping points for edges and faces together
in the new \vtk{Lagrange\rule{2em}{0.5pt}} cells.
This also makes it easy to deal with code in VTK that assumes every point defining a cell is a corner
of the cell, since the first points in the Lagrange cells are their corner points.
These are followed by mid-edge, mid-face, and finally points interior to the cell volume.

\begin{figure}
  \centering
  \includegraphics[width=2.1in]{lagrange-curve-point-layout}
  \caption{Connectivity order for Lagrange curves.}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=2.1in]{lagrange-quad-point-layout}
  \caption{Connectivity order for Lagrange quadrilaterals.}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=2.1in]{lagrange-hex-point-layout}
  \caption{Connectivity order for Lagrange hexahedra.}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=2.1in]{lagrange-wedge-point-layout}
  \caption{Connectivity order for Lagrange wedges.}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=2.1in]{lagrange-tri-point-layout}
  \caption{Connectivity order for Lagrange triangles.}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=2.1in]{lagrange-tet-point-layout}
  \caption{Connectivity order for Lagrange tetrahedra.}
\end{figure}


As you can see in the figures above,

\begin{itemize}
\item Corner vertices are reported first, in the same order as they are for the corresponding linear cell;
\item Edge, face, and volume points are then reported;
\item For prismatic shapes (curve, quadrilateral, hexahedron), points on edges are reported starting at
  the lowest $r$, $s$, or $t$ parameter value and going to the highest, even when the order the
  edges are reported in might suggest otherwise. Simlarly, for face and volume points.
\item For simplicial shapes (triangle, tetrahedron), points on edges are reported in a counterclockwise order
  which matches the order the edges themselves are presented.
  Interior points on simplicial shapes are reported after corner and edge points by treating unreported
  points as the exterior of a lower-degree element and listing corner points and edges as above.
  In this way, reporting points is like peeling an onion, as outer shells are reported before inner shells.
\item The wedge adopts the prismatic cell ordering with the exceptions that

    \begin{itemize}
    \item edge points on triangular faces are ordered counterclockwise around the triangle
    \item face points on quadrilateral faces have their first axis oriented counterclockwise
      around the triangle instead of aligned with a coordinate axis.
    \end{itemize}

  Note that it is possible to use
  \vtk{Triangle}\meth{::Index} and \vtk{Triangle}\meth{::BarycentricIndex}
  and \vtk{Wedge}\meth{::PointIndexFromIJK}
  to translate between the triangle's scheme and points in any $k = \mbox{constant}$ layer of a wedge.
  The wedge does this internally in some cases.
\end{itemize}

\section{Examples}

\begin{itemize}
\item The \vtk{CellTypeSource} filter will let you produce a grid of Lagrange cells
  with a given order. You can use this filter directly from ParaView to try it out,
  so we do not include code here.

\item The VTK XML-based readers/writers handle Lagrange cells.
  An example file is located here:
    \url{https://data.kitware.com/api/v1/file/5a3a87af8d777f5e872f60a2/download}
  which has some elements with curved shape and all of them with an ellipsoidal
  scalar function defined over each cell that illustrates an important feature
  of higher-order cells: the ability to have interior minima, maxima, and other
  critical points. See Figure~\ref{f:HexesQuadsWedge} for an illustration.

\begin{figure}[tbh]
  \centering
  \includegraphics[width=3.2in]{HexesQuadsWedge}
  \caption{\label{f:HexesQuadsWedge}%
    A wireframe outline of several Lagrange cells
    (2 hexahedra, 2 quadrilateral, and 1 wedge) along
    with a series of isocontours of an ellipsoidal
    scalar function.
  }
\end{figure}


\item To create individual cells and add them to an unstructured grid, see
  the example python code in Listing~\ref{l:CreateATetrahedron}.

\item Beyond these examples, you may be interested in the unit tests for the
  new cell types.
    \begin{itemize}
    \item Some C++ unit tests are in \texttt{Common/DataModel/Testing/Cxx}:
        \begin{itemize}
        \item LagrangeInterpolation.cxx: Shape function evaluation common to the "tensor product" shapes (curve, quad, hex, and wedge).
        \item TestLagrangeTetra.cxx, TestLagrangeTriangle.cxx, LagrangeHexahedron.cxx: Unit tests of inherited cell functions.
        \end{itemize}
    \item Python integration tests are in \texttt{Filters/Geometry/Testing/Python/LagrangeGeometricOperations.py}.
      In particular, this test demonstrates:
        \begin{itemize}
        \item reading in the new cells from an XML file;
        \item intersecting cells with lines and glyphing the resulting points; and
        \item running filters on unstructured grids containing the new cells:
          isocontouring, clipping, and cutting.
        \end{itemize}
    \end{itemize}
\end{itemize}

\begin{longlisting}
  \begin{minted}{c}
import math
import vtk

# Let's make a sixth-order tetrahedron
order = 6
# The number of points for a sixth-order tetrahedron is
nPoints = (order + 1) * (order + 2) * (order + 3) / 6;

# Create a tetrahedron and set its number of points. Internally, Lagrange cells
# compute their order according to the number of points they hold.
tet = vtk.vtkLagrangeTetra()
tet.GetPointIds().SetNumberOfIds(nPoints)
tet.GetPoints().SetNumberOfPoints(nPoints)
tet.Initialize()

point = [0.,0.,0.]
barycentricIndex = [0, 0, 0, 0]

# For each point in the tetrahedron...
for i in range(nPoints):
    # ...we set its id to be equal to its index in the internal point array.
    tet.GetPointIds().SetId(i, i)

    # We compute the barycentric index of the point...
    tet.ToBarycentricIndex(i, barycentricIndex)

    # ...and scale it to unity.
    for j in range(3):
        point[j] = float(barycentricIndex[j]) / order

    # A tetrahedron comprised of the above-defined points would have straight
    # edges. Let's deform them a bit using a warping field.
    dist = 0.
    for j in range(3):
        dist = dist + point[j] * point[j]
    dist = math.sqrt(dist) / math.sqrt(3.)
    p = [ 0., 0., 0. ]
    for j in range(3):
        p[j] = point[j] - (.1 - .2 * math.sin(dist * 4. * math.pi))/order

    # Now that we have our warped point, set the point values:
    tet.GetPoints().SetPoint(i, p[0], p[1], p[2])

# Add the tetrahedron to a cell array
tets = vtk.vtkCellArray()
tets.InsertNextCell(tet)

# Add the points and tetrahedron to an unstructured grid
uGrid = vtk.vtkUnstructuredGrid()
uGrid.SetPoints(tet.GetPoints())
uGrid.InsertNextCell(tet.GetCellType(), tet.GetPointIds())

# Visualize
mapper = vtk.vtkDataSetMapper()
mapper.SetInputData(uGrid)

actor = vtk.vtkActor()
actor.SetMapper(mapper)

renderer = vtk.vtkRenderer()
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)

renderer.AddActor(actor)
renderer.SetBackground(.2, .3, .4)

renderWindow.Render()
renderWindowInteractor.Start()
  \end{minted}
  \caption{\label{l:CreateATetrahedron}%
    A Python example illustrating how to create a
    Lagrange tetrahedron.}
\end{longlisting}


\section{Helpful tips}

\begin{itemize}
\item Some static allocation is done by the interpolation and cell classes, which fixes a compile-time
  maximum order of $10$ along each axis.  For $3$-D elements, this would result in $O(1000)$ points per cell.
\item Static allocation is also used by the \vtk{UnstructuredGridGeometryFilter}, which likewise limits
  the filter to running on cells of order 10 or lower. The \macro{VTK\_MAXIMUM\_NUMBER\_OF\_POINTS} constant
  in its source can be changed at compile time to allow higher degrees but at a significant cost.
\item Be aware that the vtkLagrangeWedge class reports all of its faces with outward-pointing normals
  just like most other cells in VTK. Unfortunately, vtkWedge does not follow this pattern.
\end{itemize}

\section{Acknowledgments}

The authors would like to thank SAIC for funding this work and generously licensing it so others can benefit from it.
