# Arbitrary-order Isoparametric Lagrange Finite Elements in VTK and ParaView

To generate a PDF for this article, run

```sh
pdflatex -shell-escape lagrange
```

You must have the python Pygments package installed and the LaTeX minted package
as they are used to format the Python source code.

The meat of the article's text is in `article.tex`.
